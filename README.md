ics-ans-role-docker
===================

Ansible role to install docker on CentOS.

The docker-py Python library is also installed to support the docker Ansible modules
(docker_container, docker_image, docker_network).
Note that docker-compose is not installed, so docker_service is not supported.

Requirements
------------

- ansible >= 2.7
- molecule >= 2.20

Role Variables
--------------

```yaml
docker_version: 20.10.6
docker_cli_version: 20.10.6
docker_containerd_io_version: 1.4.4
docker_public_repository: false
docker_users: []
docker_log_driver: json-file
docker_log_opts: {}
# The default storage driver if used by default
# Define the following variable to force it
# docker_storage_driver: "overlay2"
docker_cron_task: true
# Define the following variable to set the ip range used by Docker networks
# docker_default_address_pools:
#   - base: 172.21.0.0/16
#     size: 24
docker_proxy_url: http://proxy:8080
docker_registry_mirrors: []
```

The users defined in the ``docker_users`` variable will be added to the docker group.
You can set the ``docker_storage_driver`` to force a specific storage driver.

Example Playbook
----------------

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-docker
      docker_compose_version: none
```

License
-------

BSD 2-clause
