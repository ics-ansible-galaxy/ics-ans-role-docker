# Generic docker tests
import os
import re
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_docker_running_and_enabled(host):
    docker = host.service("docker")
    assert docker.is_running
    assert docker.is_enabled


def test_docker_run(host):
    with host.sudo():
        cmd = host.command("docker run hello-world")
    assert cmd.rc == 0
    assert "Hello from Docker!" in cmd.stdout


def test_docker_python_library_installed(host):
    if host.ansible.get_variables()["inventory_hostname"].endswith("-ubuntu"):
        cmd = host.run("python3 -c 'import docker; print(docker.__version__)'")
        assert cmd.rc == 0
        assert cmd.stdout.strip() == "4.1.0"
    else:
        cmd = host.run("python -c 'import docker; print(docker.__version__)'")
        assert cmd.rc == 0
        assert cmd.stdout.strip() == "1.10.6"


def test_docker_log_driver(host):
    with host.sudo():
        cmd = host.run("docker info")
        if host.ansible.get_variables()["inventory_hostname"].endswith("ubuntu"):
            logfile = "/var/log/syslog"
        else:
            logfile = "/var/log/messages"
        if "ics-ans-role-docker-daemon-config" in host.ansible.get_variables()["inventory_hostname"]:
            assert "Logging Driver: syslog" in cmd.stdout
            assert re.search(
                r"[0-9a-fA-F]{12}\[[\d]+\]: Hello from Docker!",
                host.file(logfile).content_string,
            )
        else:
            assert "Logging Driver: json-file" in cmd.stdout
            assert "Hello from Docker!" not in host.file(logfile).content_string


def test_docker_storage_driver(host):
    with host.sudo():
        config = host.file("/etc/docker/daemon.json")
        if host.ansible.get_variables()["inventory_hostname"].startswith("ics-ans-role-docker-daemon-config"):
            cmd = host.run("docker info")
            assert "Storage Driver: overlay" in cmd.stdout
            assert config.contains('"storage-driver": "overlay"')
        else:
            assert not config.contains('"storage-driver":')


def test_docker_default_address_pool(host):
    with host.sudo():
        cmd = host.run("docker network inspect --format '{{(index .IPAM.Config 0).Subnet}}' bridge")
        if host.ansible.get_variables()["inventory_hostname"].startswith("ics-ans-role-docker-daemon-config"):
            assert cmd.stdout.strip() == "172.21.0.0/24"
        else:
            assert cmd.stdout.strip() == "172.17.0.0/16"


def test_docker_registry_mirror(host):
    with host.sudo():
        cmd = host.run("docker info --format '{{(index .RegistryConfig.IndexConfigs \"docker.io\").Mirrors}}'")
        if host.ansible.get_variables()["inventory_hostname"].startswith("ics-ans-role-docker-daemon-config"):
            assert cmd.stdout.strip() == "[https://docker.esss.lu.se/]"
        else:
            assert cmd.stdout.strip() == "[]"


def test_docker_ipv6(host):
    with host.sudo():
        cmd = host.run("docker network inspect --format '{{(index .IPAM.Config 1).Subnet}}' bridge")
        assert cmd.stdout.strip() == "fd00:17::/64"
